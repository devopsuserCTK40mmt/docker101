# docker101

Docker 101, a project by Team 4 for class of Network Administration (INT3310)

# Team members:
- Hung Nguyen
- Lam Dao
- Nga Nguyen
- Son Nguyen
- Thang Phan
- Trung Dinh

# Documents

- Slide: https://www.slideshare.net/ssuser9b325a/docker-101-144718472
- Report: [Docker101.pdf](docs/Docker101.pdf)